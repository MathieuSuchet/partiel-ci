import os
import pickle
import socket
from threading import Thread
import numpy as np
from pandas import read_csv
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from typing import List

class Model(object):
    N_CHARS = 20

    def __init__(self) -> None:
        self.model = LogisticRegression()

        if os.path.exists("saves/model"):
            self.load()
        else:
            self.__init_data()
            self.__init_learning()

        self.connect()

    def save(self, filepath = "saves/model"):
        with open(filepath, "wb") as f:
            pickle.dump(self.model, f)

    def load(self, filepath = "saves/model"):
        with open(filepath, "rb") as f:
            self.model = pickle.load(f)

    def __encode(self, data: List[str]):
        encoded_data = [[]] * len(data)

        for i, crit in  enumerate(data):
            n_crit = [0] * Model.N_CHARS
            for j in range(Model.N_CHARS):
                n_crit[j] = ord(crit[j])
            encoded_data[i] = n_crit

        return encoded_data

    def __init_data(self):
        data = read_csv("reviews_unique.csv", sep=",")
        data.dropna()
        data.drop_duplicates()

        critiques: List[str] = data["critiques"].tolist()
        labels = data["labels"].tolist()

        N_CHARS = 20

        encoded_critiques = [[]] * len(critiques)
        for i, crit in  enumerate(critiques):
            n_crit = [0] * N_CHARS
            for j in range(N_CHARS):
                n_crit[j] = ord(crit[j])
            encoded_critiques[i] = n_crit

        X_train, X_test, y_train, y_test = train_test_split(encoded_critiques, labels, train_size=0.8, test_size=0.2)
        self.train_data = (X_train, y_train)
        self.test_data = (X_test, y_test)

    def __init_learning(self):

        model = LogisticRegression()
        model.fit(*self.train_data)
        self.save()

    def predict(self, input):
        print("Inputs")
        print("Input : ", input)
        encoded_inputs = self.__encode(input)
        return self.model.predict(np.array(encoded_inputs).reshape((len(encoded_inputs), Model.N_CHARS))).tolist()

    def __client_thread(self, s):
            data = pickle.loads(s.recv(1204))
            print(data)
            s.send(pickle.dumps(self.predict(data["inputs"])))
            s.close()

    def connect(self):
        s = socket.socket(socket.AF_INET)
        s.bind(("0.0.0.0", 5000))
        s.listen()
        
        while True:
            try:
                client_socket, _ = s.accept()
                Thread(target=self.__client_thread, args=(client_socket, )).start()
            except Exception as e:
                print(e)
                s.close()


if __name__ == "__main__":
    Model()
