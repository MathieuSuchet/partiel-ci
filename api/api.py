from argparse import ArgumentParser
from http import HTTPStatus
import os
import pickle
import socket
from flask import Flask, Response, request

app = Flask(__name__)

@app.post("/predict")
def predict():
    body = request.get_json()

    print(body)

    if "type" not in body.keys() or "inputs" not in body.keys():
        for i in body["inputs"]:
            if len(i) < 20: 
                return Response("Invalid format", HTTPStatus.NOT_ACCEPTABLE)

    s = socket.socket(socket.AF_INET)
    s.connect((os.environ["MODEL_HOST"], 5000))
    s.send(pickle.dumps(body))
    result = pickle.loads(s.recv(1024))

    return {"result": result}
    
if __name__ == "__main__":

    parser = ArgumentParser("Api", "api.py", "The api process")
    args = parser.parse_args()
    app.run(host="0.0.0.0")